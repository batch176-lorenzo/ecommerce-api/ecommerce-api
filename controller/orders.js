const Order = require("../model/Order");
const Product = require("../model/Product");

const total = async (total, cart) => {
	for (let index = 0; index < cart.length; index++) {
		await Product.findById(cart[index].productId)
			.then((product) => {
				if (product.isActive) {
					total += product.price * cart[index].quantity;
				} else {
					total = null;
					return total;
				}
			})
			.catch((error) => error.message);
	}
	return total;
};

// Non-admin User checkout 
module.exports.order = async (req, res) => {
	if (req.user.isAdmin) {
		return res.send({ message: "Action forbidden" });
	} else {
		let cart = req.body;
		if (cart.length) {
			
			let total = 0;

			let getTotalAmount = await toal(total, cart);

			if (getTotalAmount) {
				let newOrderDetails = {
					userId: req.user.id,
					totalAmount: getTotalAmount,
				};

				let newOrder = new Order(newOrderDetails);

				cart.forEach((product) => newOrder.products.push(product));

				return newOrder.save().then((result) => res.send(result)).catch((error) => res.send(error.message));
			} else {
				return Promise.reject({ message: "Can't process order" }).then((result) => res.send(result)).catch((error) => res.send(error.message));
			}
		} else {
		
			return Product.findById(cart.productId)
				.then((product) => {
					if (product.isActive === false) {
						return new Promise((resolve, reject) => {
							reject({ message: "Product is not active" });
						});
					} else {
						let total = cart.quantity * product.price;

						let newOrderDetails = {
							userId: req.user.id,
							totalAmount: total,
						};

						let newOrder = new Order(newOrderDetails);

						newOrder.products.push(cart);

						return newOrder.save().then((result) => res.send(result)).catch((error) => res.send(error.message));
					}
				})
				.catch((error) => res.send(error.message));
		}
	}
};

// Retrieve all orders Admin only
module.exports.getAllOrders = async (req, res) => {
	return Order.find({}).then((result) => res.send(result)).catch((err) => res.send(err.message));
};

