const Products = require('../model/Product');

module.exports.addProduct = (reqBody) => {
	let newProduct = new Products({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newProduct.save().then((product, error) => {
		if (product) {
			return product
		} else {
			return error
		}
	})
};

module.exports.getAllProducts =() => {
	return Products.find({}).then(result => {
		return result;
	})
}

module.exports.getAllActive = () => {
	return Products.find({isActive: true}).then(result => {
		return result
	})
}

module.exports.getProduct = (reqParam) => {
	return Products.findById(reqParam).then(result => {
		return result
	}).catch(error => error);
};

module.exports.updateProduct = (productId, update) => {
	let updatedProduct = {
		name: update.name,
		description: update.description,
		price: update.price
	}

	return Products.findByIdAndUpdate(productId, updatedProduct).then((product, error) =>{
		if (error) {
			return error
		} else {	
			return product
		}
	}).catch(error => error)
};

// Archiving a Product

	module.exports.archiveProduct = (productId) => {
		let updateActiveField = {
			isActive : false
		};
		return Products.findByIdAndUpdate(productId, updateActiveField).then((course, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		}).catch(error => error)
	};