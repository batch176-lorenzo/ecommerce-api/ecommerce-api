//[SECTION] Dependencies and Modules
	const User = require('../model/User');	
	const Product =require('../model/Product')
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require('../auth');


//[SECTION] Environment Setup
	dotenv.config();
	const salt = parseInt(process.env.SALT);

//[SECTION] Functionalites [CREATE]
	module.exports.register = (userData) => {
		let email = userData.email;
		let passW = userData.password
		let newUser = new User({
			email: email,
			password: bcrypt.hashSync(passW, salt),
		})
		return newUser.save().then((user, err)=>{
			if (user) {
				return user
			} else {
				return 'Failed to Register account'
			};
		});
	}; 
//[SECTION] USER AUTHENTICATION
	module.exports.userLogin = (data) =>{
		return User.findOne({email: data.email}).then(result => {
			if (result == null) {
				return false;
			} else {
				const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)
				if (isPasswordCorrect) {
					return {accessToken: auth.createAccessToken(result.toObject())}
				} else {
					return false;
				};
			};
		});
	};	 


	module.exports.getProfile = (data) => {
		return User.findById(data.userId).then(result => {
			result.password = ''
			return result
		}).catch(error => error)
	}

//[SECTION] SETTING USER AS ADMIN

	module.exports.updateAdmin = (userId, reqBody) => {
		let updatedStatus = {
			isAdmin: true
		}
		return User.findByIdAndUpdate(userId, updatedStatus).then((status, error) =>{
			if (error) {
				return error
			} else {
				return status
			}
		}).catch(error => error);
	}	

// //ORDER Registered User

// 	module.exports.order = async (req, res) => {
// 		console.log(req.body.productName);
// 		console.log(req.body.quantity);
// 		console.log(req.user.id)

// 		if (req.user.isAdmin){
// 			return res.send({message : "Action Forbidden"})
// 		}

// 		let isUserUpdated = await User.findById(req.user.id).then(user => {
// 			let newOrder = ({
// 				productName: req.body.productName,
// 				quantity: req.body.quantity
// 			})
// 			user.orders.push(newOrder);
// 			return user.save().then(user => true).catch(error => error.message);

// 			if (isUserUpdated !== true) {
// 				return res.send({message: isUserUpdated})
// 			}
// 		});

// 		let isProductUpdate = await Product.findById(req.user.orderId).then(product =>{
// 			let order = {
// 				orderId: req.user.orderId
// 			}

// 			product.orders.push(order)
// 		})

// 		if (isUserUpdated){
//  		return res.send({meessage: "Enrolled Succesfully"})
//  	}
// 	}	


