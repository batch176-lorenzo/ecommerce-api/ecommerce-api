//[SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controller/users');
	const auth = require('../auth')
	
//[SECTION]	Routing Component
	const route = exp.Router();
//[SECTION]	Route POST
	route.post('/register', (req, res) => {
		let userData = req.body;
		controller.register(userData).then(result => {
			res.send(result)
		});
	});
//[SECTION] Route for User Authentication
	route.post('/login', (req,res) => {
		controller.userLogin(req.body).then(result => res.send(result));
	})

	route.get('/details', auth.verify, (req, res) => {
		controller.getProfile({userId: req.body.id}).then(result => res.send(result))
	})	
	
//Set User as Admin
	route.put('/:userId', auth.verify, auth.verifyAdmin, (req, res) =>{
		controller.updateAdmin(req.params.userId, req.body).then(result =>res.send(result));
	});

//Creating an Order

	// route.post('/order', auth.verify, controller.order);
//[SECTION] Expose Route System
	module.exports = route; 