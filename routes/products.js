const express = require('express');
const route = express.Router();
const ProductsController = require('../controller/products');
const auth = require('../auth');
const {verify, verifyAdmin} = auth



//CREATE PRODUCT ADMIN ONLY
route.post('/create',verify,verifyAdmin,(req, res) =>{
	ProductsController.addProduct(req.body).then(result => res.send(result))
})
//RETRIEVE ALL PRODUCTS ADMIN ONLY
route.get('/all', verify, verifyAdmin, (req, res) => {
	ProductsController.getAllProducts().then(result => res.send(result))
});
// retrieve all ACTIVE products
route.get('/active', (req, res) => {
	ProductsController.getAllActive().then(result => res.send(result));
});

// retrieveing a SPECIFIC products
route.get('/:productId', (req, res) => {
	ProductsController.getProduct(req.params.productId).then(result =>res.send(result));
});

//UPDATE PRODUCT INFORMATION ADMIN ONLY
route.put('/:productId',verify, verifyAdmin, (req, res) => {
	ProductsController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
});
module.exports = route;

//Archiving a course

route.put('/:productId/archive', verify, verifyAdmin, (req,res) => {
	ProductsController.archiveProduct(req.params.productId).then(result => res.send(result));
});