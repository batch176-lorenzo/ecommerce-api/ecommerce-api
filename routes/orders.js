const express = require("express");
const controller = require("../controller/orders");
const auth = require("../auth");

const routes = express.Router();

const { verify, verifyAdmin } = auth;

// Non-admin User checkout 
routes.post("/order", verify, controller.order);

// Retrieve all orders Admin only
routes.get("/all", verify, verifyAdmin, controller.getAllOrders);

module.exports = routes;
