//[SECTION]	Modules and Dependencies
	const mongoose  = require('mongoose');
//[SECTION]	Schema/Blueprint
	const productsSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, 'Name is Required']
		},
		description: {
			type: String,
			required: [true, 'Description is Required']
		},
		price: {
			type: Number,
			required: [true, 'Name is Required']
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		}
	});
//[SECTION]	Model   
	module.exports = mongoose.model('Product', productsSchema);